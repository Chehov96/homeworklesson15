$(document).ready(function () {
   $('.slider__body').slick({
      speed: 1000,
      autoplay: true,
      autoplaySpeed: 5000,
      touchThreshold: 10,
      slidesToShow: 1,
      responsive: [
         {
            breakpoint: 768,
            settings: {
               arrows: false
            }
         }]
   });
   $('.slider__body').slick('setPosition');
});

$(document).ready(function () {
   $('.header__burger').click(function (event) {
      $('.header__burger,.header__burgerLine,.header__burgerLine_firstLine,.header__burgerLine_lastLine').toggleClass('active');
      $('.header__navigationMenu').toggleClass('active');
      $('.header__logo').toggleClass('active');
   });
});